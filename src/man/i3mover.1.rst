i3mover
#######

Interactively moves i3wm containers and workspaces
==================================================

:author: Paul Mullen
:contact: pm@nellump.net
:copyright: © 2021-2022 Paul Mullen <pm@nellump.net>
:manual_section: 1
:version: 1.1.2
:date: 2022-06-07



Synopsis
--------

i3mover [-h] [-v] [-V] [-n] [-g] -c|-C|-w|-W



Description
-----------

``i3mover`` interactively moves i3wm containers and workspaces from one
workspace or output to another.  It makes it easy to:

- move the active container to another workspace or output
- move the active workspace to another output
- move multiple workspaces to another output



Options
-------

-h  Print usage
-v  Print version
-V  Verbose mode
-n  Dry run mode
-c  Move focused container to another workspace
-C  Move focused container to another output
-w  Move focused workspace to another output
-W  Move multiple workspaces to another output



Exit Status
-----------

:0:  Success
:1:  Error



Examples
--------

``i3mover`` is most useful when paired with suitable i3wm keybindings.
Consider addding something like the following to your i3wm configuration
file:

.. code:: config   

   bindsym $mod+c exec i3mover -c
   bindsym $mod+w exec i3mover -w
   bindsym $mod+shift+W exec i3mover -W
