#!/bin/bash

# Interactively move i3wm containers and workspaces between workspaces
# or outputs.

# Copyright (c) 2021-2022 Paul Mullen (pm@nellump.net)
# SPDX-License-Identifier: MIT





# Important Stuff
#

set -u  # Disallow unset variables and parameters.
#set -e  # Exit on non-zero command status (errors).





# Constants
#

readonly myname="$(basename "$0")"
readonly version="1.1.2"
readonly usage_short="$myname [-h] [-v] [-V] [-n] [-g] -c|-C|-w|-W"
readonly usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -c    Move focused container to another workspace
    -C    Move focused container to another output
    -w    Move focused workspace to another output
    -W    Move multiple workspaces to another output"

readonly exit_success=0
readonly exit_error=1

# - Zenity treats these as suggestions.
# - If these aren't set global, bats whitebox test fails w/ "unbound"
#   errors.  Why?
declare -gri zenity_dialog_width=200
declare -gri zenity_dialog_min_height=150





# Defaults
#

verbose=0
dry_run=0
mode=""





# Subroutines
#


echo_verbose() {
    local -r output="$@"

    if [[ $verbose -eq 1 ]] ; then
        echo "$output" >&2
    fi
}



# Low-Level Operations
#

get_outputs_all() {
    # Return newline-delimited list of connected outputs.

    xrandr | grep ' conn' | cut -f 1 -d ' '
}



get_workspaces_json() {
    # Return JSON of workspace data provided by i3.

    i3-msg -t get_workspaces
}



get_output_focused() {
    # Return the name of the X11 output that holds the currently-focused
    # workspace.

    get_workspaces_json | jq '.[] | select(.focused == true) | .output' | tr -d \"
}



get_outputs_unfocused() {
    # Return newline-delimited list of names of all X11 outputs other
    # than that which holds the currently-focused workspace.

    local focused_output=$(get_output_focused)
    get_outputs_all | while read output ; do
        echo $output | grep -v "$focused_output" >/dev/null
        [[ $? -eq 0 ]] && echo $output
    done
}



get_output_primary() {
    # Return the name of the primary X11 output.

    xrandr | grep ' conn' | while read output ; do
        echo $output | grep -i primary >/dev/null
        if [[ $? -eq 0 ]] ; then
            echo $output | cut -f 1 -d ' '
            break
        fi
    done
}



translate_output_alias() {
    # Take the name of an X11 output and, if it's an alias, translate it
    # to its actual name.  I.e., "primary" will be translated to e.g.,
    # "HDMI-1".
    #
    # NB: Use this function as close to the final use of $output as
    # possible (e.g., selection dialogs, i3-msg calls, etc.).

    local output=$1

    if [[ $output == "primary" ]] ; then
        echo $(get_output_primary)
    else
        echo $output
    fi
}



add_alias_to_output_list() {
    # Take a newline-delimited list of X11 output names and insert
    # alias(es), if appropriate.  E.g., if the primary output is
    # "LVDS-1", an input list of "LVDS-1\nHDMI-1" would be returned as
    # "primary\nLVDS-1\nHDMI-1".

    local outputs_raw=$1

    local primary_output=$(get_output_primary)
    echo "$outputs_raw" | while read output ; do
        [[ $output == $primary_output ]]  &&  echo "primary"
        echo "$output"
    done
}



get_workspaces_all() {
    # Return newline-delimited list of names of all i3 workspaces.

    local query=". | sort_by(.num) | .[].name"
    get_workspaces_json | jq "$query" | tr -d \"
}



get_workspace_focused() {
    # Return name of the currently-focused i3 workspace (i.e., that
    # which contains the currently-focused container).

    local query=".[] | select(.focused == true) | .name"
    get_workspaces_json | jq "$query" | tr -d \"
}



get_workspaces_unfocused() {
    # Return newline-delimited list of names of i3 workspaces other than
    # the currently-focused workspace.

    local query=".[] | select(.focused != true) | .name"
    get_workspaces_json | jq "$query" | tr -d \"
}



get_workspaces_not_on_output() {
    # Take an X11 output name and return newline-delimited list of all
    # i3 workspaces *not* on that output.

    local output=$(translate_output_alias $1)

    local query=". | sort_by(.num) | .[] | select(.output != \"$output\") | .name"
    get_workspaces_json | jq "$query" | tr -d \"
}



# GUI Business
#

get_selection_dmenu() {
    local prompt=$1
    local items=$2

    echo "$items" |  dmenu -i -p $prompt
}



get_selection_zenity() {
    local -i return_normal=0
    local -i return_canceled=1
    local -i return_error=2

    # Defaults
    multi=''

    local opt OPTIND OPTARG OPTERR
    while getopts ":m" opt; do
      case $opt in
        m)
          local multi="--multiple"
          ;;
        \?)
          echo "Invalid option: $OPTARG" >&2
          return $return_error
          ;;
      esac
    done

    shift $((OPTIND-1))
    local prompt=$1
    shift

    local items="$@"

    # Calculate GUI dialog height
    local IFS=$'\n'
    # For some reason, $# doesn't work here (always == 1):
    local -ir items_qty=$(echo "$items" | grep -c $'\n')
    # Zenity dialog always has room for this many items:
    local -ir items_qty_gui_min=3
    # Increase dialog height by a factor of this:
    local -i items_qty_gui_extra
    local -i gui_item_height=25
    if [[ $items_qty -lt $items_qty_gui_min ]] ; then
        items_qty_gui_extra=0
    else
        items_qty_gui_extra=$((items_qty - items_qty_gui_min))
    fi
    local -ir gui_height=$((zenity_dialog_min_height + items_qty_gui_extra*gui_item_height))

    # Present dialog and get selection.
    # Redirect Zenity's various warnings to /dev/null.
    local selection
    selection=$(zenity --list  --text="$prompt" $multi \
                       --width $zenity_dialog_width --height $gui_height \
                       --column Name  --hide-header \
                       $items 2>/dev/null)

    if [[ $? -ne 0 ]] ; then
        return $return_canceled
    elif [[ -z $selection ]] ; then
        return $selection
    else
        echo $selection |  tr '|' '\n'
    fi
}



show_info_zenity(){
    local msg="$1"

    zenity --info --width $zenity_dialog_width --text "$msg" 2>/dev/null
}



get_selection_single_from_unfocused_outputs() {
    # Present user with dmenu-based selection of unfocused X11 outputs,
    # from which they can selection one.

    local -i return_normal=0
    local -i return_canceled=1
    local -i return_empty=2

    local outputs_raw=$(get_outputs_unfocused)
    [[ -z $outputs_raw ]]  &&  return $return_empty

    local outputs_cooked=$(add_alias_to_output_list "$outputs_raw")
    local output
    output=$(get_selection_dmenu "OUTPUT:" "$outputs_cooked")
    if [[ $? -ne 0 ]] ; then
        return $return_canceled
    else
        echo "$output"
    fi
}



get_selection_single_from_all_outputs() {
    # Present user with zenity-based selection of all X11 outputs, from
    # which they can select one.

    local -i return_normal=0
    local -i return_canceled=1
    local -i return_empty=2

    local outputs_raw=$(get_outputs_all)
    # Any less than two available outputs and there's nothing to do.
    if [[ $(echo "$outputs_raw" | wc -l) -lt 2 ]] ; then
        return $return_empty
    fi

    local outputs_cooked=$(add_alias_to_output_list "$outputs_raw")
    local output
    output=$(get_selection_zenity "Select destination output:" "$outputs_cooked")
    if [[ $? -ne 0 ]] ; then
        return $return_canceled
    else
        echo "$output"
    fi
}



get_selection_single_from_unfocused_workspaces() {
    local -i return_normal=0
    local -i return_canceled=1
    local -i return_empty=2

    local unfocused_workspaces_names=$(get_workspaces_unfocused)
    [[ -z $unfocused_workspaces_names ]]  &&  return $return_empty

    local workspace
    workspace=$(get_selection_dmenu "WORKSPACE:" "$unfocused_workspaces_names")
    if [[ $? -eq 1 ]] ; then
        return $return_canceled
    else
        echo "$workspace"
    fi
}



# i3wm Operations
#

switch_workspace(){
    # Switch focus from currently-focused i3 workspace to another.

    local workspace=$1

    local cmd="i3-msg workspace \"$workspace\""
    [[ $dry_run -eq 1 ]]  &&  echo "D> $cmd"  ||  eval "$cmd >/dev/null"
}



move_focused_container_to_workspace() {
    # Move currently-focused i3 container to another i3 workspace.

    local workspace=$1

    local cmd="i3-msg move container to workspace \"$workspace\""
    [[ $dry_run -eq 1 ]]  &&  echo "D> $cmd"  ||  eval "$cmd 2>/dev/null"
}



move_focused_container_to_output() {
    # Move currently-focused i3 container to another X11 output.

    local output=$1

    local cmd="i3-msg move container to output \"$(translate_output_alias $output)\""
    [[ $dry_run -eq 1 ]]  &&  echo "D> $cmd"  ||  eval "$cmd 2>/dev/null"
}



move_focused_workspace_to_output() {
    # Move currently-focused i3 workspace to another X11 output.

    local output=$1

    local cmd="i3-msg move workspace to output \"$(translate_output_alias $output)\""
    # Redirect i3-msg noisy status output to /dev/null.
    [[ $dry_run -eq 1 ]]  &&  echo "D> $cmd"  ||  eval "$cmd >/dev/null"
}



# High-Level Operations
#

move_focused_workspace_to_output_carefully() {
    # Move currently-focused i3 workspace to another X11 output,
    # preceded by a few necessary sanity checks.  A wrapper around
    # move_focused_workspace_to_output().

    local workspace_name_to_move=$1
    local output=$2

    # Switch to the workspace to be moved, if necessary.
    local currently_focused_workspace_name=$(get_workspace_focused)
    if [[ $workspace_name_to_move != $currently_focused_workspace_name ]] ; then
        echo_verbose "Switching from workspace \"$currently_focused_workspace_name\" to \"$workspace_name_to_move\""
        switch_workspace "$workspace_name_to_move"
    fi

# NB: Shouldn't ever get here, given earlier checks.
#   # Maybe we're already here?
#   local focused_output=$(get_focused_output)
#   if [[ "$(translate_output_alias $output)" == "$focused_output" ]] ; then
#       echo_verbose "Workspace \"$workspace_name_to_move\" already on output \"$focused_output\"."
#       show_info_zenity "$comment"
#       return
#   fi

    # Move that workspace!
    echo_verbose "Moving workspace \"$workspace_name_to_move\" to output \"$output\""
    move_focused_workspace_to_output $output
}



move_focused_container_to_new_workspace() {
    # Get workspace selection.
    local dest_workspace
    dest_workspace="$(get_selection_single_from_unfocused_workspaces)"
    local -i ret=$?
    local comment
    if [[ $ret -eq 1 ]] ; then
        echo_verbose "Selection canceled."
        exit $exit_success
    elif [[ $ret -eq 2 ]] ; then
        comment="No other workspacess available."
        show_info_zenity "$comment"
        echo_verbose "$comment"
        exit $exit_success
    fi

    # Move container to new workspace.
    echo_verbose "Moving focused container to workspace \"$dest_workspace\""
    move_focused_container_to_workspace "$dest_workspace"
}



move_focused_container_to_new_output() {
    # Get output selection.
    local dest_output
    dest_output="$(get_selection_single_from_unfocused_outputs)"
    local -i ret=$?
    local comment
    if [[ $ret -eq 1 ]] ; then
        echo_verbose "Selection canceled."
        exit $exit_success
    elif [[ $ret -eq 2 ]] ; then
        comment="No other outputs available."
        show_info_zenity "$comment"
        echo_verbose "$comment"
        exit $exit_success
    fi

    # Move container to new output.
    echo_verbose "Moving focused container to output \"$dest_output\""
    move_focused_container_to_output "$dest_output"
}



move_focused_workspace_to_new_output() {
    # Get output selection.
    local dest_output
    dest_output="$(get_selection_single_from_unfocused_outputs)"
    local -i ret=$?
    local comment
    if [[ $ret -eq 1 ]] ; then
        echo_verbose "Selection canceled."
        exit $exit_success
    elif [[ $ret -eq 2 ]] ; then
        comment="No other outputs available."
        show_info_zenity "$comment"
        echo_verbose "$comment"
        exit $exit_success
    fi

    # Move workspace to new output.
    # (move_focused_workspace_to_output_carefully() emits verbose output,
    # so we don't have to.)
    move_focused_workspace_to_output_carefully "$(get_workspace_focused)" "$dest_output"
}



move_selected_workspaces_to_new_output() {
    # Get output selection.
    local dest_output
    dest_output="$(get_selection_single_from_all_outputs)"
    local -i ret=$?
    local comment
    if [[ $ret -eq 1 ]] ; then
        echo_verbose "Selection canceled."
        exit $exit_success
    elif [[ $ret -eq 2 ]] ; then
        comment="No other outputs available."
        show_info_zenity "$comment"
        echo_verbose "$comment"
        exit $exit_success
    fi
    comment="Output selected: \"$dest_output\""
    [[ $verbose -eq 1 ]]  &&  echo $comment >&2

    # Get list of workspaces available to be moved to the selected output.
    local workspaces_not_on_dest_output=$(get_workspaces_not_on_output "$dest_output")
    if [[ -z $workspaces_not_on_dest_output ]] ; then
        comment="All workspaces already on output \"$dest_output\"."
        show_info_zenity "$comment"
        echo_verbose "$comment"
        exit $exit_success
    fi

    # Get workspace(s) selection.
    local workspaces_to_move
    workspaces_to_move=$(get_selection_zenity -m "Select workspaces to move:" "$workspaces_not_on_dest_output")
    ret=$?
    if [[ $ret -eq 1 ]] ; then
        echo_verbose "Selection canceled."
        exit $exit_success
    fi
    echo_verbose "Workspaces selected: ${workspaces_to_move/$'\n'/, }"

    # Get originally focused workspace name before making any i3-msg calls.
    originally_focused_workspace_name=$(get_workspace_focused)

    # Move workspaces to new output.
    echo "$workspaces_to_move" | while read workspace ; do
        move_focused_workspace_to_output_carefully "$workspace" "$dest_output"
    done

    # Switch back to the originally focused workspace.
    local currently_focused_workspace_name=$(get_workspace_focused)
    if [[ $currently_focused_workspace_name != $originally_focused_workspace_name ]] ; then
        comment="Switching from current workspace \"$currently_focused_workspace_name\" "
        comment+="back to originally focused workspace, \"$originally_focused_workspace_name\""
        echo_verbose "$comment"
        switch_workspace "$originally_focused_workspace_name"
    fi
}



main() {
    # Process command line options.
    while getopts ":hvVncCwW" opt; do
        case $opt in
            h)
                echo "$usage_long"  # Need quotes to preserve newlines!
                exit $exit_success
                ;;
            v)
                echo "$myname $version"
                exit $exit_success
                ;;
            V)
                verbose=1
                echo "Verbose mode requested." >&2
                ;;
            n)
                dry_run=1
                echo "Dry run mode requested." >&2
                ;;
            c)
                mode="container-to-workspace"
                ;;
            C)
                mode="container-to-output"
                ;;
            w)
                mode="workspace-to-output"
                ;;
            W)
                mode="workspaces-to-output"
                ;;
            \?)
                echo "Invalid option: $OPTARG"
                echo $usage_short >&2
                exit $exit_error
                ;;
        esac
    done

    shift $((OPTIND-1))

    # Check for unconsumed args.
    if [[ $@ ]]; then
        echo "Unconsumed arguments: '$@'"
        echo $usage_short >&2
        exit $exit_error
    fi

    # Ensure $mode is set.
    if [[ -z $mode ]]; then
        echo $usage_short >&2
        exit $exit_error
    fi


    case $mode in
        container-to-workspace)
            move_focused_container_to_new_workspace
            ;;

        container-to-output)
            move_focused_container_to_new_output
            ;;

        workspace-to-output)
            move_focused_workspace_to_new_output
            ;;

        workspaces-to-output)
            move_selected_workspaces_to_new_output
            ;;

        *)
            # Shouldn't ever end up here.
            echo "No matching mode."
            exit
            ;;
    esac
}





if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main $@
fi
