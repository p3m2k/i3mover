v.1.1.2 (2022-05-30)
===============================================================================
581d2aea   Import Makefile "install" target fix


v.1.1.1 (2022-05-30)
===============================================================================
38d4d5e0   Fix Makefile "install" target but
d8827469   Increase height and width of GUI dialogs
e673d396   Test program w/ "verbose" option enabled in whitebox test suite


v.1.1.0 (2022-05-23)
===============================================================================
113bafe7   Display dialog when there's nothing i3mover can do


v.1.0.0 (2022-05-23)
===============================================================================
Initial release.
