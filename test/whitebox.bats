load lib/utils
load lib/assertions
load lib/mocker

load requirements


setup() {
    mk_test_tmpdir
    mock="mock -d $BATS_TEST_TMPDIR"
    canned="test/canned"

    # NB: Can't be `local`:
    i3mover=$(which i3mover)
    source $i3mover
#   echo "# Loading $i3mover" >&3

    verbose=1
}


teardown() {
    rm_test_tmpdir
}



# Low-Level Operations
#

@test "get_outputs_all(), normal" {
    $mock xrandr "$(cat $canned/xrandr.out)"

    run get_outputs_all

    assert_lines_qty 2
    [ "${lines[0]}" = "LVDS-1" ]
    [ "${lines[1]}" = "HDMI-1" ]
}


@test "get_outputs_all(), no connected outputs" {
    $mock xrandr "$(cat $canned/xrandr-no_connected_output.out)"

    run get_outputs_all

    assert_lines_qty 0
}


# @test "get_workspaces_json()" {
#     # Nothing much to test here.
# }


@test "get_output_focused()" {
    $mock get_workspaces_json "$(cat $canned/workspaces.json)"

    run get_output_focused

    assert_lines_qty 1
    [ "${lines[0]}" = "LVDS-1" ]
}


@test "get_output_focused(), no focused outputs" {
    # Shouldn't be possible, but...
    $mock get_workspaces_json "$(cat $canned/workspaces-no_focused_workspace.json)"

    run get_output_focused

    assert_lines_qty 0
}


@test "get_outputs_unfocused()" {
    $mock get_output_focused "$(echo "HDMI-1")"
    $mock get_outputs_all "$(echo "LVDS-1" ; echo "HDMI-1")"

    run get_outputs_unfocused

    assert_lines_qty 1
    [ "${lines[0]}" = "LVDS-1" ]
}


@test "get_outputs_unfocused(), no unfocused outputs" {
    $mock get_output_focused "$(echo "HDMI-1")"
    $mock get_outputs_all "$(echo "HDMI-1")"

    run get_outputs_unfocused

    assert_lines_qty 0
}


@test "get_output_primary()" {
    $mock xrandr "$(cat $canned/xrandr.out)"

    run get_output_primary

    assert_lines_qty 1
    [ "${lines[0]}" = "LVDS-1" ]
}


@test "get_output_primary(), no primary output" {
    # Shouldn't be possible, but...
    $mock xrandr "$(cat $canned/xrandr-no_connected_output.out)"

    run get_output_primary

    assert_lines_qty 0
}


@test "translate_output_alias(), primary" {
    $mock get_output_primary "$(echo "LVDS-1")"

    run translate_output_alias "primary"

    assert_lines_qty 1
    [ "${lines[0]}" = "LVDS-1" ]
}


@test "translate_output_alias(), not primary" {
    run translate_output_alias "HDMI-1"

    assert_lines_qty 1
    [ "${lines[0]}" = "HDMI-1" ]
}


@test "add_alias_to_output_list()" {
    $mock get_output_primary "$(echo "LVDS-1")"

    list=$(echo "LVDS-1" ; echo "HDMI-1" ; echo "HDMI-2")
    run add_alias_to_output_list "$list"

    assert_lines_qty 4
    [ "${lines[0]}" = "primary" ]
    [ "${lines[1]}" = "LVDS-1" ]
    [ "${lines[2]}" = "HDMI-1" ]
    [ "${lines[3]}" = "HDMI-2" ]
}


@test "add_alias_to_output_list(), no primary" {
    $mock get_output_primary "$(echo "LVDS-1")"

    list=$(echo "HDMI-1" ; echo "HDMI-2")
    run add_alias_to_output_list "$list"

    assert_lines_qty 2
    [ "${lines[0]}" = "HDMI-1" ]
    [ "${lines[1]}" = "HDMI-2" ]
}


@test "get_workspaces_all()" {
    $mock get_workspaces_json "$(cat $canned/workspaces.json)"

    run get_workspaces_all

    assert_lines_qty 3
    [ "${lines[0]}" = "1 one" ]
    [ "${lines[1]}" = "2 too" ]
    [ "${lines[2]}" = "3 three" ]
}


@test "get_workspace_focused()" {
    $mock get_workspaces_json "$(cat $canned/workspaces.json)"

    run get_workspace_focused

    assert_lines_qty 1
    [ "${lines[0]}" = "1 one" ]
}


@test "get_workspaces_unfocused()" {
    $mock get_workspaces_json "$(cat $canned/workspaces.json)"

    run get_workspaces_unfocused

    assert_lines_qty 2
    [ "${lines[0]}" = "2 too" ]
    [ "${lines[1]}" = "3 three" ]
}


@test "get_workspaces_not_on_output()" {
    $mock get_workspaces_json "$(cat $canned/workspaces.json)"

    run get_workspaces_unfocused LVDS-1

    assert_lines_qty 2
    [ "${lines[0]}" = "2 too" ]
    [ "${lines[1]}" = "3 three" ]
}



# GUI Business
#

# @test "get_selection_dmenu()" {
#     # Nothing much to test here.
# }


@test "get_selection_zenity(), single, normal" {
    prompt='Choose and Perish!'
    items=$(echo "PICK ME!" ; echo "2 Too" ; echo "Free")

    run get_selection_zenity "$prompt" "$items"

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "PICK ME!" ]
}


@test "get_selection_zenity(), single, none selected" {
    prompt='Choose and Perish!'
    items=$(echo "PICK NONE!" ; echo "2 Too" ; echo "Free")

    run get_selection_zenity "$prompt" "$items"

    assert_retval 0
    assert_lines_qty 0
}


@test "get_selection_zenity(), single, selection canceled" {
    prompt='Choose and Perish!'
    items=$(echo "CANCEL THIS!" ; echo "2 Too" ; echo "Free")

    run get_selection_zenity "$prompt" "$items"

    assert_retval 1
    assert_lines_qty 0
}


@test "get_selection_zenity(), multi, normal" {
    prompt='Choose and Perish!'
    items=$(echo "PICK ME!" ; echo "PICK ME TOO!" ; echo "Free";
            echo "another" ; echo "yet another" ; echo "even more another!!")

    run get_selection_zenity -m "$prompt" "$items"

    assert_retval 0
    assert_lines_qty 2
    [ "${lines[0]}" = "PICK ME!" ]
    [ "${lines[1]}" = "PICK ME TOO!" ]
}


@test "get_selection_zenity(), invalid option" {
    prompt='Unimportant'
    items='Nobody Cares'

    run get_selection_zenity -z "$prompt" "$items"

    assert_retval 2
    assert_lines_qty 1
    [ "${lines[0]}" = "Invalid option: z" ]
}


# @test "show_info_zenity()" {
#     # Nothing much to test here.
# }


@test "get_selection_single_from_unfocused_outputs(), normal" {
    local fake_outputs_unfocused="$(echo "HDMI-1" ; echo "HDMI-2")"
    $mock get_outputs_unfocused "$fake_outputs_unfocused"

    $mock add_alias_to_output_list \
        "$(echo "PICK ME!" ; echo "$fake_outputs_unfocused")"
    local add_alias_args_expected="$fake_outputs_unfocused"

    run get_selection_single_from_unfocused_outputs

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "PICK ME!" ]
    assert_file_contents "$MOCK_ADD_ALIAS_TO_OUTPUT_LIST_CALLS_DIR/1.txt" \
                         "$add_alias_args_expected"
}


@test "get_selection_single_from_unfocused_outputs(), no unfocused outputs" {
    $mock get_outputs_unfocused "$(echo)"

    run get_selection_single_from_unfocused_outputs

    assert_retval 2
    assert_lines_qty 0
}


@test "get_selection_single_from_unfocused_outputs(), selection canceled" {
    local fake_outputs_unfocused="$(echo "HDMI-1" ; echo "HDMI-2")"
    $mock get_outputs_unfocused "$fake_outputs_unfocused"

    $mock add_alias_to_output_list \
        "$(echo "CANCEL THIS!" ; echo "$fake_outputs_unfocused")"
    local add_alias_args_expected="$fake_outputs_unfocused"

    run get_selection_single_from_unfocused_outputs

    assert_retval 1
    assert_lines_qty 0
    assert_file_contents "$MOCK_ADD_ALIAS_TO_OUTPUT_LIST_CALLS_DIR/1.txt" \
                         "$add_alias_args_expected"
}


@test "get_selection_single_from_all_outputs(), normal" {
    local fake_outputs_all="$(echo "HDMI-1" ; echo "HDMI-2")"
    $mock get_outputs_all "$fake_outputs_all"

    $mock add_alias_to_output_list \
        "$(echo "PICK ME!" ; echo "$fake_outputs_all")"
    local add_alias_args_expected="$fake_outputs_all"

    run get_selection_single_from_all_outputs

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "PICK ME!" ]
    assert_file_contents "$MOCK_ADD_ALIAS_TO_OUTPUT_LIST_CALLS_DIR/1.txt" \
                         "$add_alias_args_expected"
}


@test "get_selection_single_from_all_outputs(), one output" {
    local fake_outputs_all="$(echo "HDMI-1")"
    $mock get_outputs_all "$fake_outputs_all"

    run get_selection_single_from_all_outputs

    assert_retval 2
    assert_lines_qty 0
}


@test "get_selection_single_from_all_outputs(), no outputs" {
    $mock get_outputs_all "$(echo)"

    run get_selection_single_from_all_outputs

    assert_retval 2
    assert_lines_qty 0
}


@test "get_selection_single_from_all_outputs(), selection canceled" {
    local fake_outputs_all="$(echo "HDMI-1" ; echo "HDMI-2")"
    $mock get_outputs_all "$fake_outputs_all"

    $mock add_alias_to_output_list \
        "$(echo "CANCEL THIS!" ; echo "$fake_outputs_all")"
    local add_alias_args_expected="$fake_outputs_all"


    run get_selection_single_from_all_outputs

    assert_retval 1
    assert_lines_qty 0
    assert_file_contents "$MOCK_ADD_ALIAS_TO_OUTPUT_LIST_CALLS_DIR/1.txt" \
                         "$add_alias_args_expected"
}


@test "get_selection_single_from_unfocused_workspaces(), normal" {
    $mock get_workspaces_unfocused "$(echo "PICK ME!" ; echo "2 too")"

    run get_selection_single_from_unfocused_workspaces

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "PICK ME!" ]
}


@test "get_selection_single_from_unfocused_workspaces(), no unfocused workspaces" {
    $mock get_workspaces_unfocused "$(echo)"

    run get_selection_single_from_unfocused_workspaces

    assert_retval 2
    assert_lines_qty 0
}


@test "get_selection_single_from_unfocused_workspaces(), selection canceled" {
    $mock get_workspaces_unfocused "$(echo "CANCEL THIS!" ; echo "2 too")"

    run get_selection_single_from_unfocused_workspaces

    assert_retval 1
    assert_lines_qty 0
}



# i3wm Operations
#

@test "switch_workspace()" {
    $mock i3-msg
    local i3msg_args_expected="workspace MY_NEW_WORKSPACE"

    run switch_workspace MY_NEW_WORKSPACE

    assert_retval 0
    assert_lines_qty 0
    assert_file_contents "$MOCK_I3_MSG_CALLS_DIR/1.txt" "$i3msg_args_expected"
}


@test "move_focused_container_to_workspace()" {
    $mock i3-msg
    local i3msg_args_expected="move container to workspace MY_NEW_WORKSPACE"

    run move_focused_container_to_workspace MY_NEW_WORKSPACE

    assert_retval 0
    assert_lines_qty 0
    assert_file_contents "$MOCK_I3_MSG_CALLS_DIR/1.txt" "$i3msg_args_expected"
}


@test "move_focused_container_to_output()" {
    $mock i3-msg
    local i3msg_args_expected="move container to output MY_NEW_OUTPUT"

    run move_focused_container_to_output MY_NEW_OUTPUT

    assert_retval 0
    assert_lines_qty 0
    assert_file_contents "$MOCK_I3_MSG_CALLS_DIR/1.txt" "$i3msg_args_expected"
}


@test "move_focused_workspace_to_output()" {
    $mock i3-msg
    local i3msg_args_expected="move workspace to output MY_NEW_OUTPUT"

    run move_focused_workspace_to_output MY_NEW_OUTPUT

    assert_retval 0
    assert_lines_qty 0
    assert_file_contents "$MOCK_I3_MSG_CALLS_DIR/1.txt" "$i3msg_args_expected"
}



# High-Level Operations
#

@test "move_focused_workspace_to_output_carefully(), no switch workspace" {
    local workspace_to_move="5 Five"
    local output_to_move_to="HDMI-2"

    local fake_workspace_focused="$workspace_to_move"
    $mock get_workspace_focused "$fake_workspace_focused"

    $mock move_focused_workspace_to_output
    local move_focused_workspace_args_expected="$output_to_move_to"

    run move_focused_workspace_to_output_carefully \
        "$workspace_to_move" "$output_to_move_to"

    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CALLS_DIR/1.txt" \
                         "$move_focused_workspace_args_expected"
}


@test "move_focused_workspace_to_output_carefully(), switch workspace" {
    local workspace_to_move="5 Five"
    local output_to_move_to="HDMI-2"

    local fake_workspace_focused="$(echo "LVDS-1")"
    $mock get_workspace_focused "$fake_workspace_focused"

    $mock switch_workspace
    local switch_workspace_args_expected="$workspace_to_move"

    $mock move_focused_workspace_to_output
    local move_focused_workspace_args_expected="$output_to_move_to"

    run move_focused_workspace_to_output_carefully \
        "$workspace_to_move" "$output_to_move_to"

    assert_file_contents "$MOCK_SWITCH_WORKSPACE_CALLS_DIR/1.txt" \
                         "$switch_workspace_args_expected"
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CALLS_DIR/1.txt" \
                         "$move_focused_workspace_args_expected"
}



@test "move_focused_container_to_new_workspace(), normal" {
    local fake_workspace_selection="5 Five"
    $mock get_selection_single_from_unfocused_workspaces "$fake_workspace_selection"

    $mock move_focused_container_to_workspace
    local move_focused_container_args_expected="$fake_workspace_selection"

    run move_focused_container_to_new_workspace

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Moving focused container to workspace \"$fake_workspace_selection\"" ]
    assert_file_contents "$MOCK_MOVE_FOCUSED_CONTAINER_TO_WORKSPACE_CALLS_DIR/1.txt" \
                         "$move_focused_container_args_expected"
}


@test "move_focused_container_to_new_workspace(), no other workspaces" {
    $mock -r 2 get_selection_single_from_unfocused_workspaces

    run move_focused_container_to_new_workspace

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "No other workspacess available." ]
}


@test "move_focused_container_to_new_workspace(), selection canceled" {
    $mock -r 1 get_selection_single_from_unfocused_workspaces

    run move_focused_container_to_new_workspace

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Selection canceled." ]
}



@test "move_focused_container_to_new_output(), normal" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_unfocused_outputs "$fake_output_selection"

    $mock move_focused_container_to_output
    local move_focused_container_args_expected="$fake_output_selection"

    run move_focused_container_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Moving focused container to output \"$fake_output_selection\"" ]
    assert_file_contents "$MOCK_MOVE_FOCUSED_CONTAINER_TO_OUTPUT_CALLS_DIR/1.txt" \
                         "$move_focused_container_args_expected"
}


@test "move_focused_container_to_new_output(), no other outputs" {
    $mock -r 2 get_selection_single_from_unfocused_outputs

    run move_focused_container_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "No other outputs available." ]
}


@test "move_focused_container_to_new_output(), selection canceled" {
    $mock -r 1 get_selection_single_from_unfocused_outputs

    run move_focused_container_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Selection canceled." ]
}



@test "move_focused_workspace_to_new_output(), normal" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_unfocused_outputs "$fake_output_selection"

    $mock move_focused_workspace_to_output
    local move_focused_workspace_args_expected="$fake_output_selection"

    run move_focused_workspace_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Moving workspace \"3 Dev\" to output \"$fake_output_selection\"" ]
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CALLS_DIR/1.txt" \
                         "$move_focused_workspace_args_expected"
}


@test "move_focused_workspace_to_new_output(), no other outputs" {
    $mock -r 2 get_selection_single_from_unfocused_outputs

    run move_focused_workspace_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "No other outputs available." ]
}


@test "move_focused_workspace_to_new_output(), selection canceled" {
    $mock -r 1 get_selection_single_from_unfocused_outputs

    run move_focused_workspace_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Selection canceled." ]
}



@test "move_selected_workspaces_to_new_output(), no switch workspace" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_all_outputs "$fake_output_selection"

    local fake_workspaces_avail="$(echo "1 One" ; echo "2 Too" ; echo "3 Free")"
    $mock get_workspaces_not_on_output "$fake_workspaces_avail"
    local get_workspaces_args_expected="$fake_output_selection"

    local fake_workspaces_selection="$(echo "1 One" ; echo "2 Too")"
    $mock get_selection_zenity "$fake_workspaces_selection"
    local get_selection_zenity_args_expected="-m Select workspaces to move: $fake_workspaces_avail"

    local original_workspace="3 Free"
    local final_workspace="3 Free"
    $mock get_workspace_focused "$original_workspace"
    $mock get_workspace_focused "$final_workspace"

    $mock move_focused_workspace_to_output_carefully

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 2
    [ "${lines[0]}" = "Output selected: \"$fake_output_selection\"" ]
    [ "${lines[1]}" = "Workspaces selected: ${fake_workspaces_selection/$'\n'/, }" ]
    assert_file_contents "$MOCK_GET_WORKSPACES_NOT_ON_OUTPUT_CALLS_DIR/1.txt" \
                         "$get_workspaces_args_expected"
    assert_file_contents "$MOCK_GET_SELECTION_ZENITY_CALLS_DIR/1.txt" \
                         "$get_selection_zenity_args_expected"
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CAREFULLY_CALLS_DIR/1.txt" \
                         "1 One $fake_output_selection"
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CAREFULLY_CALLS_DIR/2.txt" \
                         "2 Too $fake_output_selection"

}


@test "move_selected_workspaces_to_new_output(), normal, switch workspace" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_all_outputs "$fake_output_selection"

    local fake_workspaces_avail="$(echo "1 One" ; echo "2 Too" ; echo "3 Free")"
    $mock get_workspaces_not_on_output "$fake_workspaces_avail"
    local get_workspaces_args_expected="$fake_output_selection"

    local fake_workspaces_selection="$(echo "1 One" ; echo "2 Too")"
    $mock get_selection_zenity "$fake_workspaces_selection"
    local get_selection_zenity_args_expected="-m Select workspaces to move: $fake_workspaces_avail"

    local original_workspace="1 One"
    local final_workspace="3 Free"
    $mock get_workspace_focused "$original_workspace"
    $mock get_workspace_focused "$final_workspace"

    $mock move_focused_workspace_to_output_carefully

    $mock switch_workspace
    local switch_workspace_args_expected="$original_workspace"

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 3
    [ "${lines[0]}" = "Output selected: \"$fake_output_selection\"" ]
    [ "${lines[1]}" = "Workspaces selected: ${fake_workspaces_selection/$'\n'/, }" ]
    [ "${lines[2]}" = "Switching from current workspace \"$final_workspace\" back to originally focused workspace, \"$original_workspace\"" ]
    assert_file_contents "$MOCK_GET_WORKSPACES_NOT_ON_OUTPUT_CALLS_DIR/1.txt" \
                         "$get_workspaces_args_expected"
    assert_file_contents "$MOCK_GET_SELECTION_ZENITY_CALLS_DIR/1.txt" \
                         "$get_selection_zenity_args_expected"
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CAREFULLY_CALLS_DIR/1.txt" \
                         "1 One $fake_output_selection"
    assert_file_contents "$MOCK_MOVE_FOCUSED_WORKSPACE_TO_OUTPUT_CAREFULLY_CALLS_DIR/2.txt" \
                         "2 Too $fake_output_selection"
    assert_file_contents "$MOCK_SWITCH_WORKSPACE_CALLS_DIR/1.txt" \
                         "$switch_workspace_args_expected"
}


@test "move_selected_workspaces_to_new_output(), output selection canceled" {
    $mock -r 1 get_selection_single_from_all_outputs

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Selection canceled." ]
}


@test "move_selected_workspaces_to_new_output(), no other outputs" {
    $mock -r 2 get_selection_single_from_all_outputs

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 1
    [ "${lines[0]}" = "No other outputs available." ]
}


@test "move_selected_workspaces_to_new_output(), no movable workspaces" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_all_outputs "$fake_output_selection"

    $mock get_workspaces_not_on_output
    local get_workspaces_args_expected="$fake_output_selection"

    $mock show_info_zenity
    local comment="All workspaces already on output \"$fake_output_selection\"."
    local show_info_args_expected="$comment"

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 2
    [ "${lines[0]}" = "Output selected: \"$fake_output_selection\"" ]
    [ "${lines[1]}" = "$comment" ]
    assert_file_contents "$MOCK_GET_WORKSPACES_NOT_ON_OUTPUT_CALLS_DIR/1.txt" \
                         "$get_workspaces_args_expected"
    assert_file_contents "$MOCK_SHOW_INFO_ZENITY_CALLS_DIR/1.txt" \
                         "$show_info_args_expected"
}


@test "move_selected_workspaces_to_new_output(), workspaces selection canceled" {
    local fake_output_selection="HDMI-5"
    $mock get_selection_single_from_all_outputs "$fake_output_selection"

    local fake_workspaces_avail="$(echo "1 One" ; echo "3 Free")"
    $mock get_workspaces_not_on_output "$fake_workspaces_avail"
    local get_workspaces_args_expected="$fake_output_selection"

    $mock -r 1 get_selection_zenity
    local get_selection_zenity_args_expected="-m Select workspaces to move: $fake_workspaces_avail"

    run move_selected_workspaces_to_new_output

    assert_retval 0
    assert_lines_qty 2
    [ "${lines[0]}" = "Output selected: \"$fake_output_selection\"" ]
    [ "${lines[1]}" = "Selection canceled." ]
    assert_file_contents "$MOCK_GET_WORKSPACES_NOT_ON_OUTPUT_CALLS_DIR/1.txt" \
                         "$get_workspaces_args_expected"
    assert_file_contents "$MOCK_GET_SELECTION_ZENITY_CALLS_DIR/1.txt" \
                         "$get_selection_zenity_args_expected"
}





# vim: syntax=bash
