assert_retval() {
    local -ir retval=$1

    [ "$status" -eq $retval ]
}



assert_lines_qty() {
    local -ir qty=$1

    [ "${#lines[*]}" -eq $qty ]
}



assert_file_contents() {
    local -r mock_call_record_path="$1"
    local -r mock_call_expected_str="$2"

    cmp "$mock_call_record_path" <(echo "$mock_call_expected_str")
    [ "$?" -eq 0 ]
}
