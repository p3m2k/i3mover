mk_test_tmpdir() {
    if [[ -z $BATS_RUN_TMPDIR ]] ; then
        echo "# \$BATS_RUN_TMPDIR unset!" >&3
        return 1
    fi

    BATS_TEST_TMPDIR="${BATS_RUN_TMPDIR}/${BATS_TEST_NAME}"
#   echo "# Creating test run temp dir: $BATS_TEST_TMPDIR" >&3
    mkdir -p $BATS_TEST_TMPDIR
}



rm_test_tmpdir() {
    if [[ -z $BATS_TEST_TMPDIR ]] ; then
        echo "# \$BATS_TEST_TMPDIR unset!" >&3
        return 1
    fi

#   echo "# Deleting test run temp dir: $BATS_TEST_TMPDIR" >&3
    rm -fr $BATS_TEST_TMPDIR
}



print_output() {
    echo "# lines: '${lines[*]}'" >&3
}
