f_enq() {
    local -r file="$1"  &&  shift

    local -ir return_success=0
    local -ir return_error=1

    [[ ! -e $file ]]  &&  touch $file

    local -r tmp_file=$(mktemp)
    cat $file > $tmp_file
    echo "$@" >> $tmp_file
    mv -f $tmp_file $file
}



f_deq() {
    local -r file="$1"  &&  shift

    local -ir return_success=0
    local -ir return_error=1

    [[ ! -e $file ]]  &&  return $return_error

    local -r tmp_file=$(mktemp)
    tail -n +2 $file > $tmp_file
    head -1 $file
    mv -f $tmp_file $file
}



d_enq() {
    local -r dir="$1"  &&  shift


    local -ir return_success=0
    local -ir return_error=1

    local -r file_extension='.txt'
    local -r index_file="index${file_extension}"


    if [[ ! -d "$dir" ]] ; then
        mkdir -p "$dir"
        echo "0" > "${dir}/${index_file}"
    fi


    local -r pwd="$(pwd)"
    cd "$dir"

    local -ir index_curr=$(cat "$index_file")
    local -ir index_next=$((index_curr+1))
    local -r next_file="${index_next}${file_extension}"

    echo "$@" > "$next_file"
    echo "$index_next" > "$index_file"

    cd "$pwd"
}



d_deq() {
    local -r dir="$1"  &&  shift


    local -ir return_success=0
    local -ir return_error=1
    local -ir return_empty=2

    local -r file_extension='.txt'
    local -r index_file="index${file_extension}"


    [[ -d $dir ]]  ||  return $return_error
    [[ $(ls $dir | grep -v $index_file | wc -l) -eq 0 ]]  &&  return $return_empty


    local -r pwd="$(pwd)"
    cd "$dir"

    local -r first_file="$(ls | grep -v "$index_file" | sort -n | head -1)"
    cat "$first_file"
    rm -f "$first_file"

    cd "$pwd"
}



mock() {
    local -ir return_normal=0
    local -ir return_error=1

    local -i retval=0
    local tmp_dir="/tmp"  # This must be static or predictable!

    local opt OPTIND OPTARG OPTERR
    while getopts ":d:r:" opt; do
        case $opt in
            d)
                tmp_dir="$OPTARG"
                ;;
            r)
                retval="$OPTARG"
                ;;
            \?)
                echo "Invalid option: $OPTARG" >&2
                return $return_error
                ;;
        esac
    done

    shift $((OPTIND-1))
    if [[ -z "$1" ]] ; then
        return $return_error
    else
        local -r mocked="$1"
        shift
    fi

    [[ -d "$tmp_dir" ]]  ||  return $return_error


    local -r MOCKED="$(echo $mocked | tr a-z A-Z | tr - _)"
    local -r mock_output_q_dir="${tmp_dir}/mock_${mocked}_outputs"
    local -r mock_retvals_q_file="${tmp_dir}/mock_${mocked}_retvals.txt"
    local -r mock_calls_q_dir="${tmp_dir}/mock_${mocked}_calls"

    d_enq "$mock_output_q_dir" "$@"
    f_enq "$mock_retvals_q_file" "$retval"

    # TODO: Test for pre-existing ${mocked}() ?
    local f
    f="${mocked}() {
           # Log how we were called.
           d_enq $mock_calls_q_dir \"\$@\"

           # Once the queues are emptied, output is an empty string and
           # retval is 0.
           d_deq \"$mock_output_q_dir\"
           local -ir rv=\$(f_deq \"$mock_retvals_q_file\")
           return \$rv
    }"
    eval "$f"

    export -f $mocked
    export "MOCK_${MOCKED}_CALLS_DIR"="$mock_calls_q_dir"
}



export -f f_enq f_deq d_enq d_deq
