requirements="jq dmenu zenity i3-msg"
for cmd in $requirements ; do
    which $cmd || (echo "Missing requirement: $cmd"; false) >&2
done
