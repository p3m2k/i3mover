load lib/utils
load lib/assertions
load lib/mocker

load requirements


setup() {
    mk_test_tmpdir
    mock="mock -d $BATS_TEST_TMPDIR"
    command="bash build/i3mover"

    progname="i3mover"
    version="1.1.2"
    usage_short="$progname [-h] [-v] [-V] [-n] [-g] -c|-C|-w|-W"
    usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -c    Move focused container to another workspace
    -C    Move focused container to another output
    -w    Move focused workspace to another output
    -W    Move multiple workspaces to another output"
    exit_success=0
    exit_error=1

    opts_interactive="-Vn"
}


teardown() {
    rm_test_tmpdir
}



@test "no opts, no args" {
    run $command

    assert_retval $exit_error
    assert_lines_qty 1
    [ "${lines[0]}" = "$usage_short" ]
}


@test "invalid opt" {
    run $command -z

    assert_retval $exit_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Invalid option: z" ]
    [ "${lines[1]}" = "$usage_short" ]
}


@test "help" {
    run $command -h

    assert_retval $exit_success
    assert_lines_qty 10  # bats ignores blank lines.
    [ "$output" = "$usage_long" ]
}


@test "version" {
    run $command -v

    assert_retval $exit_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$progname $version" ]
}


@test "verbose option" {
    run $command -V

    assert_retval $exit_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "$usage_short" ]
}


@test "dry run option" {
    run $command -n

    assert_retval $exit_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Dry run mode requested." ]
    [ "${lines[1]}" = "$usage_short" ]
}


@test "move focused container to new workspace" {
    $mock xrandr "$(echo)"
    $mock i3-msg "$(echo)"

    run $command $opts_interactive -c

    assert_retval $exit_success
    assert_lines_qty 3
    [ "${lines[0]}" = 'Verbose mode requested.' ]
    [ "${lines[1]}" = 'Dry run mode requested.' ]
    [ "${lines[2]}" = 'No other workspacess available.' ]
}


@test "move focused container to new output" {
    $mock xrandr "$(echo)"
    $mock i3-msg "$(echo)"

    run $command $opts_interactive -C

    assert_retval $exit_success
    assert_lines_qty 3
    [ "${lines[0]}" = 'Verbose mode requested.' ]
    [ "${lines[1]}" = 'Dry run mode requested.' ]
    [ "${lines[2]}" = 'No other outputs available.' ]
}


@test "move focused workspace to new output" {
    $mock xrandr "$(echo)"
    $mock i3-msg "$(echo)"

    run $command $opts_interactive -w

    assert_retval $exit_success
    assert_lines_qty 3
    [ "${lines[0]}" = 'Verbose mode requested.' ]
    [ "${lines[1]}" = 'Dry run mode requested.' ]
    [ "${lines[2]}" = 'No other outputs available.' ]
}


@test "move multiple workspaces to new output" {
    $mock xrandr "$(echo)"
    $mock i3-msg "$(echo)"

    run $command $opts_interactive -W

    assert_retval $exit_success
    assert_lines_qty 3
    [ "${lines[0]}" = 'Verbose mode requested.' ]
    [ "${lines[1]}" = 'Dry run mode requested.' ]
    [ "${lines[2]}" = 'No other outputs available.' ]
}





# vim: syntax=bash
