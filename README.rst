#######
i3mover
#######


Introduction
============

``i3mover`` interactively moves i3wm_ containers and workspaces from one
workspace or output to another.  It makes it easy to:

- move the active container to another workspace or output
- move the active workspace to another output
- move multiple workspaces to another output

.. _i3wm: https://i3wm.org/



Installation
============

``i3mover`` is a Bash_ shell script.  Almost every Linux system includes
Bash.  Users of other Unix-like_ systems may need to install it before
using this program.  In addition to Bash (and i3wm, naturally), it
requires several supporting programs: jq, dmenu and zenity.

There are two easy ways to install ``i3mover``:

- Packages_ are available for users of Debian and its derivatives
  (Ubuntu, etc.).  Users of Red Hat, Fedora and friends: stay tuned!

- Download an archive of the latest release and run ``make install``:

  .. code:: console

     # wget -O - https://www.nellump.net/downloads/i3mover_latest.tgz | tar xz
     # cd i3mover/
     # make install

bats_ is required to run the automated test suite (of interest only to
programmers).

.. _Bash: https://www.gnu.org/software/bash/
.. _Unix-like: http://www.linfo.org/unix-like.html
.. _packages : https://www.nellump.net/computers/free_software
.. _bats: https://github.com/bats-core/bats-core



Usage
=====

See the `manpage <src/man/i3mover.1.rst>`_.



Future Plans
============

None at this time.



Copying
=======

Everything here is copyright © 2021-2022 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
