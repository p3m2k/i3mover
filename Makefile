# Makefile for Bash scripts
# (Compatible with GNU make)
#
# Copyright (c) 2017-2022 Paul Mullen (<pm at nellump dot net>)
# SPDX-License-Identifier: MIT
#

SHELL=/bin/bash

SRC_DIR=$(CURDIR)/src
META_DIR=$(CURDIR)/meta
BUILD_DIR=$(CURDIR)/build
DEBIAN_DIR=$(CURDIR)/debian
TEST_DIR=$(CURDIR)/test
ARCHIVE_DIR=$(CURDIR)
PREFIX?=/usr/local
DESTDIR?=$(PREFIX)

STRIP_CMD=grep -vE '^$$|^\s*\#'
INSTALL_CFG=$(META_DIR)/install.txt
CAT_INSTALL_CFG=$(STRIP_CMD) $(INSTALL_CFG)

NAME=i3mover
VERSION=1.1.2

BRANCH=$(shell git branch | grep '*' | cut -d ' ' -f 2)


.PHONY: all doc build test \
        install uninstall \
        archive dpkg \
        clean distclean


all: build test

# TODO: Any good reason to make this separate from "build"?
doc:
	@echo ">>> make doc <<<"
	mkdir -p $(BUILD_DIR)
	cd $(SRC_DIR)/man/  &&  for f in *.*.rst ; do \
		man=$(BUILD_DIR)/$$(basename $$f .rst) ; \
		rst2man $$f > $$man ; \
		gzip -f $$man ; \
	done

# Stage all installable files in $(BUILD_DIR).
build: doc
	@echo ">>> make build <<<"
	mkdir -p $(BUILD_DIR)
	for f in $(SRC_DIR)/bash/* ; do \
		cp -a $$f $(BUILD_DIR)/$$(basename $$f .sh) ; \
	done

test: build
	@echo ">>> make test <<<"
#	bats --no-tempdir-cleanup $(TEST_DIR)
	bats $(TEST_DIR)


# Normally we'd like to make the "test" target a dependency of
# "install", but debuild chokes on the test cases that require an X11
# display.
install: build
	@echo ">>> make install <<<"
	$(CAT_INSTALL_CFG) | while read source dest mode owner group ; do \
		[[ -n $$owner ]]  &&  opts+="--owner $$owner " ; \
		[[ -n $$group ]]  &&  opts+="--group $$group " ; \
		[[ -n $$mode ]]   &&  opts+="--mode $$mode " ; \
		eval install --no-target-directory -D $$opts \
		             $(BUILD_DIR)/$$source $(DESTDIR)/$$dest ; \
	done

uninstall:
	@echo ">>> make uninstall <<<"
	$(CAT_INSTALL_CFG) | while read source dest owner mode ; do \
		rm -f $(PREFIX)/$$dest ; \
	done


issues:
	@echo ">>> make issues <<<"
	@echo "NB: Merge from *issues* branch first!"
	cd $(META_DIR)  &&  cil summary --is-open | grep -v '===' > $(CURDIR)/ISSUES.txt

TAG=v.$(VERSION)
release-changes:
	@echo ">>> make release-changes <<<"
	@echo "NB: Close all issues being deployed with this release first!"
	cd $(META_DIR)  &&  cil summary --label $(TAG) | grep -v '===' > $(CURDIR)/CHANGES-$(TAG).txt

archive:
	@echo ">>> make archive <<<"
# No effect unless on "master" branch.
ifeq ($(BRANCH),master)
	mkdir -p $(ARCHIVE_DIR)
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).tgz
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).zip
endif

# debuild invokes the appropriate target dependencies.
dpkg:
	@echo ">>> make dpkg <<<"
	debuild -us -uc -b


CMD_RM_LOOP=while read f ; do eval rm -rf "$$f" ; done
clean:
	@echo ">>> make clean <<<"
	$(STRIP_CMD) .gitignore | $(CMD_RM_LOOP)
	cd $(SRC_DIR)  &&  $(STRIP_CMD) .gitignore | $(CMD_RM_LOOP)
	cd $(DEBIAN_DIR)  &&  $(STRIP_CMD) .gitignore | $(CMD_RM_LOOP)

distclean: clean
	@echo ">>> make distclean <<<"
